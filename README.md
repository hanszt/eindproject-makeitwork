# Eind project MakeItWork

---

Dit is ons eindproject van het omscholingstraject MakeItWork van Cohort 15.


![picture](src/main/resources/static/images/logoo.jpg)

---

### Doel

Het doel van dit project was om met behulp van het Java Spring framework 
een webapplicatie te maken die roosters checkt voor leraren. 
Hierbij is gebruik gemaakt van verschillende technieken:

- Java Spring
- Hibernate
- AJAX
- bootstrap
- jQuery

Het is zeer leerzaam geweest om hiermee bezig te zijn geweest en levert een mooie basis
voor mijn vaardigheden als softwareontwikkelaar.

---

### Endpoints

- [Mijn inzet](http://localhost:8082/authController/login)

---
