package org.hva.repository

import org.hva.model.entity.Subject
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import kotlin.test.assertEquals

@DataJpaTest
internal open class SubjectRepositoryTest(@Autowired private val repository: SubjectRepository) {

    @Test
    fun testSubjectSortedBy() {
        val science = Subject().apply { subjectName = "Science" }
        val math = Subject().apply { subjectName = "Math" }
        val history = Subject().apply { subjectName = "History" }
        val medicine = Subject().apply { subjectName = "Medicine" }

        val subjects = listOf(science, math, history, medicine)

        repository.saveAll(subjects)

        val subjectSortedByName = repository.findAllSortedBy(Subject::getSubjectName)

        assertEquals(subjectSortedByName, listOf(history, math, medicine, science))
    }
}
