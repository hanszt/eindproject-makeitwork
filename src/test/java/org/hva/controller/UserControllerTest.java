package org.hva.controller;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
// TODO: 17-2-2023 Make this test work or remove it
@Disabled("Some of the beans can not be found. Needs to be fixed")
class UserControllerTest {

    private final UserController carRegistryController;
    private final int port;
    private final TestRestTemplate restTemplate;

    public UserControllerTest(@Autowired UserController carRegistryController,
                                       @LocalServerPort int port,
                                       @Autowired TestRestTemplate restTemplate) {
        this.carRegistryController = carRegistryController;
        this.port = port;
        this.restTemplate = restTemplate;
    }

    @Test
    void contextLoads() {
        assertThat(carRegistryController).isNotNull();
    }

    @Test
    void responseOnRequestRegistryById() {
        final var path = "/userController/userList";
        final var response = restTemplate.getForObject("http://localhost:" + port + path, List.class);

        System.out.println("response = " + response);

        assertAll(
                () -> assertThat(response).isNotNull(),
                () -> assertThat(response.size()).isZero()
        );
    }
}
