package org.hva.model;

import org.hva.model.entity.user.User;

public final class UserSingleton {

    private static UserSingleton singletonInstance;

    private User currentUser;

    // private constructor restricted to this class itself
    private UserSingleton() {
        super();
    }

    // static method to create instance of Singleton class
    public static synchronized UserSingleton getInstance() {
        if (singletonInstance == null) {
            singletonInstance = new UserSingleton();
        }
        return singletonInstance;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public User getCurrentUser() {
        return currentUser;
    }
}
