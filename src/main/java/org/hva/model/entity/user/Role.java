package org.hva.model.entity.user;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "role")
public class Role {

    @Id
    @Column(name = "role_id")
    private int id;
    @Column(name = "role_name")
    private String roleName;
    @Column(name = "role_desc")
    private String desc;

    public Role() {
        super();
    }

    public Role(int id, String name, String desc) {
        this.id = id;
        this.roleName = name;
        this.desc = desc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return String.format("Role{id=%d, roleName='%s', desc='%s'}", id, roleName, desc);
    }
}
