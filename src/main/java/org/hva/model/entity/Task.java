package org.hva.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;


@Entity
@Table(name = "Task")
public class Task {

    static final String TASK_ID_COLUMN_NAME = "task_task_id";

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = TASK_ID_COLUMN_NAME)
    private int taskId;
    private String taskName;
    private int estimatedHours;
    private int yearsToExpiryDate;


    public Task() {
    }

    public Task(int taskId, String taskName, int estimatedHours, int yearsToExpiryDate) {
        this.taskId = taskId;
        this.taskName = taskName;
        this.estimatedHours = estimatedHours;
        this.yearsToExpiryDate = yearsToExpiryDate;
    }

    public Task(int taskId) {
        this.taskId=taskId;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public int getEstimatedHours() {
        return estimatedHours;
    }

    public void setEstimatedHours(int estimatedHours) {
        this.estimatedHours = estimatedHours;
    }

    public int getYearsToExpiryDate() {
        return yearsToExpiryDate;
    }

    public void setYearsToExpiryDate(int yearsToExpiryDate) {
        this.yearsToExpiryDate = yearsToExpiryDate;
    }
}
