package org.hva.model.entity;

import jakarta.persistence.*;
import java.time.LocalDate;

@Entity
public class TaskApplication {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int userId;
    private LocalDate applicationDate;
    private LocalDate unSubscribeDate;
    private int availableHours;
    private String role;

    @ManyToOne
    @JoinColumn(name = Task.TASK_ID_COLUMN_NAME)
    private Task task;

    public TaskApplication() {
        super();
    }

    public TaskApplication(int userId, LocalDate unSubscribeDate, int availableHours, String role, Task task) {
        this.userId = userId;
        this.applicationDate = LocalDate.now();
        this.unSubscribeDate = unSubscribeDate;
        this.availableHours = availableHours;
        this.role = role;
        this.task = task;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public LocalDate getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(LocalDate applicationDate) {
        this.applicationDate = applicationDate;
    }

    public LocalDate getUnsubcribeDate() {
        return unSubscribeDate;
    }

    public void setUnsubcribeDate(LocalDate unsubcribeDate) {
        this.unSubscribeDate = unsubcribeDate;
    }

    public int getAvailableHours() {
        return availableHours;
    }

    public void setAvailableHours(int availableHours) {
        this.availableHours = availableHours;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public LocalDate getUnSubscribeDate() {
        return unSubscribeDate;
    }

    public void setUnSubscribeDate(LocalDate unSubscribeDate) {
        this.unSubscribeDate = unSubscribeDate;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }
}
