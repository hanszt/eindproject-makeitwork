package org.hva.model.entity;

import org.hva.model.UserSingleton;
import org.hva.model.entity.user.User;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class Exception {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String endDate;
    private String startDate;
    private String colorOption;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "user_id")
    private User user;

    public Exception() {
        super();
    }

    public Exception(String endDate, String startDate, String colorOption) {
        this.endDate = endDate;
        this.startDate = startDate;
        this.colorOption = colorOption;
        this.user = UserSingleton.getInstance().getCurrentUser();

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        id = id;
    }


    public String getColorOption() {
        return colorOption;
    }

    public void setColorOption(String colorOption) {
        this.colorOption = colorOption;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    @Override
    public String toString() {
        return "Exception{" +
                "user=" + user +
                '}';
    }
}
