package org.hva.model.entity;



import org.hva.model.entity.user.User;

import java.util.InputMismatchException;


public class Location implements Comparable<Location> {
    private String description;
    private int courseId;
    private User coordinator;

    
    public Location(String description, User coordinator) throws InputMismatchException{
        this.description = description;
        this.coordinator= coordinator;
    }

    @Override
    public int compareTo(Location o) {
        return this.description.compareTo(o.description);
    }
    
    public User getCoordinator() {
        return coordinator;
    }

    public int getCourseId() {
        return courseId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCoordinator(User coordinator) {
        this.coordinator = coordinator;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    @Override
    public String toString () {
        return String.format("%s, %d, %s", description, courseId, coordinator.getLastName());
    }
}
