package org.hva.model.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import org.hva.model.entity.user.User;

import java.time.LocalDate;

@Entity
public class CohortSchedule {

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   private int id;
   private String day;
   private String dayPart;
   private int weekNr;
   private LocalDate date;
   private String classRoom;
   @ManyToOne
   private User user;
   @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.REFRESH)
   private Subject subject;
   @ManyToOne
   private Cohort cohort;

   public CohortSchedule() {
      super();
   }

   public CohortSchedule(String day, String dayPart, LocalDate date, Cohort cohort) {
      this.day = day;
      this.dayPart = dayPart;
      this.date = date;
      this.cohort = cohort;
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public LocalDate getDate() {
      return date;
   }

   public void setDate(LocalDate date) {
      this.date = date;
   }

   public String getDay() {
      return day;
   }

   public void setDay(String day) {
      this.day = day;
   }

   public String getDayPart() {
      return dayPart;
   }

   public void setDayPart(String dayPart) {
      this.dayPart = dayPart;
   }

   public int getWeekNr() {
      return weekNr;
   }

   public void setWeekNr(int weekNr) {
      this.weekNr = weekNr;
   }

   public String getClassRoom() {
      return classRoom;
   }

   public void setClassRoom(String classRoom) {
      this.classRoom = classRoom;
   }

   public User getUser() {
      return user;
   }

   public void setUser(User user) {
      this.user = user;
   }

   public Subject getSubject() {
      return subject;
   }

   public void setSubject(Subject subject) {
      this.subject = subject;
   }

   public Cohort getCohort() {
      return cohort;
   }

   public void setCohort(Cohort cohort) {
      this.cohort = cohort;
   }

   @Override
   public String toString() {
      return "CohortSchedule{" +
              "id=" + id +
              ", day='" + day + '\'' +
              ", dayPart='" + dayPart + '\'' +
              ", date=" + date +
              ", user=" + user +
              ", subject=" + subject +
              ", classRoom='" + classRoom + '\'' +
              ", cohort=" + cohort +
              '}';
   }
}
