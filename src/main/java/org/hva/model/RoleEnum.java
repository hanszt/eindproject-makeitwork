package org.hva.model;

public enum RoleEnum {

    ADMIN(1, "ADMIN", "This user has admin rights"),
    TEACHER(2, "TEACHER", "This user has teacher rights"),
    COORDINATOR(3, "COORDINATOR", "this user has coordinator rights"),
    SCHEDULER(4, "SCHEDULER", "this user has scheduler rights");

    private final int id;
    private final String name;
    private final String description;

    RoleEnum(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
