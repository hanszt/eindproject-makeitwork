package org.hva.service;

import org.hva.model.entity.Exception;
import org.hva.model.UserSingleton;
import org.hva.repository.ExceptionRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ExceptionServiceImp implements ExceptionService {
    private final ExceptionRepository exceptionRepository;

    public ExceptionServiceImp(ExceptionRepository exceptionRepository) {
        this.exceptionRepository = exceptionRepository;
    }

    @Override
    public List<Exception> findAll() {
        return new ArrayList<>(exceptionRepository.findAll());
    }

    @Override
    public List<Exception> findByUserId() {
        int userId = UserSingleton.getInstance().getCurrentUser().getId();
        return new ArrayList<>(exceptionRepository.findAllByUserId(userId));
    }

    @Override
    public Exception findById(int theId) {
        Optional<Exception> result = exceptionRepository.findById(theId);
        Exception theException;
        if (result.isPresent()) {
            theException = result.get();
        } else {
            throw new RuntimeException("de incident met " + theId + " kan niet gevonden worden");
        }
        return theException;
    }

    @Override
    public void save(Exception e) {
        exceptionRepository.save(e);
    }

    @Override
    public void deleteById(int theId) {
        exceptionRepository.deleteById(theId);
    }
}
