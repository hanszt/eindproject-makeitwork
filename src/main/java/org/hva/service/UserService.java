package org.hva.service;

import org.hva.model.entity.user.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    void saveUser(User user);

    Optional<User> findByEmail(String email);

    boolean isUserAlreadyPresent(User user);

    List<User> getAllUsers();

    void updateUser(User user);

    void deleteUserById(int theId);

    User findById(int theId);

}
