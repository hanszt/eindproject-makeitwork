package org.hva.repository;

import org.hva.model.entity.Exception;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExceptionRepository extends CrudRepository<Exception, Integer> {

    @NotNull
    List<Exception> findAll();

    List<Exception> findAllByUserId(int id);

}
