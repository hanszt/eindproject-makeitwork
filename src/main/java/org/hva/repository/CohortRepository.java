package org.hva.repository;

import org.hva.model.entity.Cohort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CohortRepository extends JpaRepository<Cohort, Integer> {

    List<Cohort> findAll();

    List<Cohort> findAllByCohortIdAfter(int cohortId);

    Cohort getByCohortId(int cohortId);
}
