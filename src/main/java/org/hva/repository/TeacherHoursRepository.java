package org.hva.repository;

import org.hva.model.entity.TeacherHours;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherHoursRepository extends JpaRepository<TeacherHours, Integer> {

    TeacherHours findByUserId(int userId);

}
