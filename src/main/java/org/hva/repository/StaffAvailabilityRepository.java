package org.hva.repository;

import org.hva.model.entity.Cohort;
import org.hva.model.entity.StaffAvailability;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StaffAvailabilityRepository extends CrudRepository<StaffAvailability, Integer> {

    List<StaffAvailability> findByUser_IdAndCohort_CohortId(int id, int cohort);

    StaffAvailability findById(int id);

    List<StaffAvailability> findAllById(int id);

    StaffAvailability getStaffAvailabilityByCohortAndId(Cohort c, int id);
}
