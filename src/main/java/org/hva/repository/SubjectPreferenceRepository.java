package org.hva.repository;

import org.hva.model.entity.Subject;
import org.hva.model.entity.SubjectPreference;
import org.hva.model.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubjectPreferenceRepository extends JpaRepository<SubjectPreference, Integer> {

    SubjectPreference findBySubjectAndUser(Subject subject, User user);
}
