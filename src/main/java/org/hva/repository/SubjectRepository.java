package org.hva.repository;

import org.hva.model.entity.Subject;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.function.Function;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Integer> {

    Subject findSubjectBySubjectName(String name);

    Subject findSubjectBySubjectId(int id);

    default <K extends Comparable<? extends K>> List<Subject> findAllSortedBy(Function<Subject, ? extends K> sortSelector) {
        return findAll(Sort.sort(Subject.class).by(sortSelector));
    }
}
