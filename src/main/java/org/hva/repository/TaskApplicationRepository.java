package org.hva.repository;

import org.hva.model.entity.TaskApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskApplicationRepository extends JpaRepository<TaskApplication, Integer> {
}
