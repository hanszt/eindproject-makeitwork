package org.hva.repository;

import org.hva.model.entity.CohortSchedule;
import org.hva.model.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CohortScheduleRepository extends JpaRepository<CohortSchedule, Integer> {

    CohortSchedule findCohortScheduleByWeekNrAndDayPartAndDayAndUser(int weekNr, String dayPart, String day, User user);

    List<CohortSchedule> getAllByCohort_CohortId(int cohortId);

    List<CohortSchedule> getAllByUserIdAndSubject_SubjectIdAndCohort_CohortIdIsNot(int userId, int subjectId,int cohortId);

    List<CohortSchedule> getAllByCohort_CohortIdAndWeekNr(int cohortId, int weekNr);
}
