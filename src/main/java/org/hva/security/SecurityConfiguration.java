package org.hva.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.annotation.web.configurers.AuthorizeHttpRequestsConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityConfiguration.class);

    @Bean
    public AuthenticationManager authenticationManager(
            DataSource dataSource,
            HttpSecurity http,
            BCryptPasswordEncoder bCryptPasswordEncoder)
            throws Exception {
        LOGGER.info("--authenticationManagerBuilder is being configured...");
        return http.getSharedObject(AuthenticationManagerBuilder.class)
                .jdbcAuthentication()
                .usersByUsernameQuery("select email, password, '1' as enabled from user where email=? and status='VERIFIED'")
                .authoritiesByUsernameQuery("select u.email, r.role_name from user u inner join user_role ur " +
                        "on(u.user_id=ur.user_id) inner join role r on(ur.role_id=r.role_id) where u.email=?")
                .dataSource(dataSource)
                .passwordEncoder(bCryptPasswordEncoder)
                .and()
                .build();
    }

    @Bean
    public SecurityFilterChain injectSecurityFilterChain(HttpSecurity http,
                                                         CustomLoginSuccessHandler successHandler) throws Exception {
        return http.authorizeHttpRequests(SecurityConfiguration::configureAuthentication)
                .csrf().disable()
                .formLogin()
                .loginPage("/authController/login")
                .failureUrl("/authController/login?error=true")
                .successHandler(successHandler)
                .usernameParameter("email")
                .passwordParameter("password")
                .and()
                .logout()
                .deleteCookies("remember-me").permitAll().and().rememberMe()
                .tokenValiditySeconds(120)
                .and()
                .build();
    }

    private static void configureAuthentication(AuthorizeHttpRequestsConfigurer<HttpSecurity>
                                                        .AuthorizationManagerRequestMatcherRegistry registry) {
        registry
                .requestMatchers("/admin/**").hasAnyAuthority("ADMIN")
                .requestMatchers("/exception/**").hasAnyAuthority("TEACHER")
                .requestMatchers("/schedule/**").hasAnyAuthority("TEACHER")
                .requestMatchers("/subject/**").hasAnyAuthority("COORDINATOR")
                .requestMatchers("/cohort/**").hasAnyAuthority("ADMIN")
                .requestMatchers("/generateCohortSchedule").hasAnyAuthority("SCHEDULER")
                .requestMatchers("/home/**").hasAnyAuthority("ADMIN", "TEACHER", "COORDINATOR", "SCHEDULER", "MANAGER")
                .requestMatchers("/authController/**").permitAll()
                .requestMatchers("/login").permitAll()
                .requestMatchers("/confirm").permitAll()
                .requestMatchers("/forgot-password").permitAll()
                .requestMatchers("/confirm-reset").permitAll()
                .requestMatchers("/reset-password").permitAll()
                .requestMatchers("/register").permitAll()
                .anyRequest().authenticated();
    }

    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        return web -> web.ignoring().requestMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**");
    }
}
