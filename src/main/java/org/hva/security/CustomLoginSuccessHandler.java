package org.hva.security;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.hva.model.RoleEnum;
import org.hva.model.UserSingleton;
import org.hva.model.entity.user.Role;
import org.hva.model.entity.user.User;
import org.hva.repository.RoleRepository;
import org.hva.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import java.io.IOException;
import java.util.List;

@Configuration
public class CustomLoginSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomLoginSuccessHandler.class);
    public static final String REVOKE_PASSWORD_BECAUSE_IT_IS_COMPROMISED = "java:S6437";

    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    public CustomLoginSuccessHandler(RoleRepository roleRepository, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.encoder = passwordEncoder;
        updateRolesInDatabase();
        insertTestUserIntoDatabaseWhenNotPresent();
    }

    private void updateRolesInDatabase() {
        LOGGER.info("--Roles are being updated...");
        for (RoleEnum role : RoleEnum.values()) {
            if (!roleRepository.existsByRoleName(role.getName())) {
                roleRepository.save(new Role(role.getId(), role.getName(), role.getDescription()));
            }
        }
    }

    @SuppressWarnings(REVOKE_PASSWORD_BECAUSE_IT_IS_COMPROMISED)
    private void insertTestUserIntoDatabaseWhenNotPresent() {
        LOGGER.info("--Test user is being created...");
        List<Role> rolesList = roleRepository.findAll();
        String mail = "test@test.nl";
        String encodedPassword = encoder.encode("123456");
        LOGGER.info("--Encoded password testAdmin: {}", encodedPassword);
        User testUser = new User();
        testUser.setEmail(mail);
        testUser.setPassword(encodedPassword);
        testUser.setName("Test");
        testUser.setLastName("TestUser");
        testUser.setStatus("VERIFIED");
        testUser.setFte(1.);
        testUser.setEnabled(true);
        testUser.setRolesOfUser(rolesList);
        final var user = userRepository.findByEmail(mail).orElse(null);
        boolean userAbsent = user == null;
        LOGGER.info("Test user '{}' is {} present in database", testUser, userAbsent ? "not" : "already");
        if (userAbsent) {
            userRepository.save(testUser);
            LOGGER.info("--{} is added", testUser);
        } else {
            LOGGER.info("--Test user not added");
        }
    }

    @Override
    protected void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        LOGGER.info("--Login being handled...");
        setCurrentUser(authentication);
        String targetUrl = "/welcomeScreen";
        LOGGER.info("--{}", targetUrl);
        LOGGER.info("--ServletRequest: {}", request);
        LOGGER.info("--ServletResponse: {}", response);
        if (response.isCommitted()) {
            return;
        }
        RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
        redirectStrategy.sendRedirect(request, response, targetUrl);
    }

    private void setCurrentUser(Authentication authentication) {
        String emailCurrentUser = authentication.getName();
        User currentUser = userRepository.findByEmail(emailCurrentUser).orElseThrow();
        UserSingleton.getInstance().setCurrentUser(currentUser);
    }
}
