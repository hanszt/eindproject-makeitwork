package org.hva.controller;

import org.hva.model.entity.user.Role;
import org.hva.model.entity.user.User;
import org.hva.repository.RoleRepository;
import org.hva.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static java.util.Comparator.comparing;

@Controller
@RequestMapping("/userController")
public class UserController {

    private static final String ROLES = "roles";
    private final UserService userService;
    private final RoleRepository roleRepository;

    public UserController(UserService userService, RoleRepository roleRepository) {
        this.userService = userService;
        this.roleRepository = roleRepository;
    }

    @PostMapping(value = "/update")
    public String update(@ModelAttribute("user") @Valid User user) {
        userService.updateUser(user);
        return "redirect:/userController/userList";
    }

    @RequestMapping("/userList")
    public String getAllUsers(Model model) {
        List<Role> roleList = roleRepository.findAll();
        List<User> userList = userService.getAllUsers();
        userList.sort(comparing(User::getName, String::compareToIgnoreCase));
        model.addAttribute(ROLES, roleList);
        model.addAttribute("users", userList);
        return "user/users";
    }

    @GetMapping("/showFormForAdd")
    public String showFormForAdd(Model model) {
        User user = new User();
        List<Role> roleList = roleRepository.findAll();
        model.addAttribute(ROLES, roleList);
        model.addAttribute("user", user);
        return "user/newUser";
    }

    @GetMapping("/showFormForUpdate")
    public String showFormForUpdate(@RequestParam("userId") int id, Model model) {
        List<Role> roleList = roleRepository.findAll();
        model.addAttribute(ROLES, roleList);
        User theUser = userService.findById(id);
        model.addAttribute("user", theUser);
        return "user/updateUser";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("userId") int id) {
        userService.deleteUserById(id);
        return "redirect:/userController/userList";
    }
}
