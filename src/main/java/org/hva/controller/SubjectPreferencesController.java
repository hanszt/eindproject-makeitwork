package org.hva.controller;

import jakarta.servlet.http.HttpServletRequest;
import org.hva.model.UserSingleton;
import org.hva.model.entity.Subject;
import org.hva.model.entity.SubjectPreference;
import org.hva.model.entity.user.User;
import org.hva.repository.SubjectPreferenceRepository;
import org.hva.repository.SubjectRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Hans Zuidervaart
 */
@Controller
public class SubjectPreferencesController {

    private final SubjectRepository subjectRepository;
    private final SubjectPreferenceRepository subjectPreferenceRepository;

    public SubjectPreferencesController(SubjectPreferenceRepository subjectPreferenceRepository, SubjectRepository subjectRepository) {
        this.subjectPreferenceRepository = subjectPreferenceRepository;
        this.subjectRepository = subjectRepository;
    }

    @RequestMapping(value = "/showSubjects")
    public String getSubjectList(Model model) {
        Map<Subject, Integer> subjectAndPreferenceMap = new LinkedHashMap<>();
        List<Subject> subjectList = subjectRepository.findAllSortedBy(Subject::getSubjectId);
        User user = UserSingleton.getInstance().getCurrentUser();
        for (Subject subject : subjectList) {
            SubjectPreference subjectPreference = subjectPreferenceRepository.findBySubjectAndUser(subject, user);
            Integer preference = subjectPreference != null ? subjectPreference.getPreference() : null;
            subjectAndPreferenceMap.put(subject, preference);
        }
        model.addAttribute("showSubjectsWithPreference", subjectAndPreferenceMap);
        return "teacherSubjectPreferences";
    }

    @PostMapping(value = "/submitPreferences")
    public String submitPreferences(HttpServletRequest request, Model model) {
        Enumeration<String> paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            String subjectName = paramNames.nextElement();
            String[] paramValues = request.getParameterValues(subjectName);
            int subjectPreference = Integer.parseInt(paramValues[0]);
            Subject subject = subjectRepository.findSubjectBySubjectName(subjectName);
            User user = UserSingleton.getInstance().getCurrentUser();
            subjectPreferenceRepository.save(new SubjectPreference(user, subjectPreference, subject));
        }
        return getSubjectList(model);
    }
}
