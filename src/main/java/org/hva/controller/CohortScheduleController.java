package org.hva.controller;

import jakarta.servlet.http.HttpServletRequest;
import org.hva.model.entity.Cohort;
import org.hva.model.entity.CohortSchedule;
import org.hva.model.entity.Exception;
import org.hva.model.entity.StaffAvailability;
import org.hva.model.entity.Subject;
import org.hva.model.entity.TeacherHours;
import org.hva.model.entity.user.User;
import org.hva.repository.CohortRepository;
import org.hva.repository.CohortScheduleRepository;
import org.hva.repository.ExceptionRepository;
import org.hva.repository.StaffAvailabilityRepository;
import org.hva.repository.SubjectPreferenceRepository;
import org.hva.repository.SubjectRepository;
import org.hva.repository.TeacherHoursRepository;
import org.hva.repository.UserRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;


/**
 * @author Brahim Etthogri
 */
@RestController
public class CohortScheduleController {

    public static final String DEFAULT = "default";
    private final CohortScheduleRepository cohortScheduleRepo;
    private final CohortRepository cohortRepository;
    private final UserRepository userRepo;
    private final SubjectRepository subjectRepository;
    private final SubjectPreferenceRepository subjectPreferenceRepository;
    private final TeacherHoursRepository teacherHoursRepository;
    private final StaffAvailabilityRepository staffAvailrepo;
    private final ExceptionRepository exceptionRepo;

    public CohortScheduleController(CohortScheduleRepository cohortScheduleRepo,
                                    CohortRepository cohortRepository,
                                    UserRepository userRepo,
                                    SubjectRepository subjectRepository,
                                    SubjectPreferenceRepository subjectPreferenceRepository,
                                    TeacherHoursRepository teacherHoursRepository,
                                    StaffAvailabilityRepository staffAvailrepo,
                                    ExceptionRepository exceptionRepo) {
        this.cohortScheduleRepo = cohortScheduleRepo;
        this.cohortRepository = cohortRepository;
        this.userRepo = userRepo;
        this.subjectRepository = subjectRepository;
        this.subjectPreferenceRepository = subjectPreferenceRepository;
        this.teacherHoursRepository = teacherHoursRepository;
        this.staffAvailrepo = staffAvailrepo;
        this.exceptionRepo = exceptionRepo;
    }

    private String totalCheck(int cohortId,
                              int teacherId,
                              String dayPart,
                              String weekDay,
                              int subjectId,
                              int weekNumber) {
        String avail = checkGeneralAvail(teacherId, weekDay, dayPart, cohortId);
        String overlap = checkCohortOverlap(cohortId, teacherId, weekNumber, dayPart, weekDay);
        String incident = checkTeacherIncident(teacherId, weekNumber);

        final var s = checkSubjectPreference(teacherId, subjectId);
        String subjectPref = "NON".equals(s) ? "OK" : s;
        String checkHours = checkTeacherHours(teacherId, subjectId, cohortId);
        String hours = "OK".equals(checkHours) || "NON".equals(checkHours) ? "OK" : "NOK";
        //check if availability is OK or NOK
        String dayAvail = isOk(avail, overlap, incident) ? "OK" : "NOK";
        String total = hours + "-" + dayAvail + "-" + subjectPref;

        return switch (total) {
            case "OK-OK-OK" -> "OK";
            case "NOK-NOK-NOK" -> "NOK";
            case "NOK-OK-OK" -> "hourNOK_restOK";
            case "OK-NOK-OK" -> "availNOK_restOK";
            case "OK-OK-NOK" -> "subjectNOK_restOK";
            case "NOK-NOK-OK" -> "hoursNOK_availNOK_OK";
            case "OK-NOK-NOK" -> "OK_availNOK_prefNOK";
            case "NOK-OK-NOK" -> "hoursNOK_OK_prefNOK";
            default -> DEFAULT;
        };
    }

    private static boolean isOk(String avail, String overlap, String incident) {
        final var b = "NOK".equals(avail) &&
                "OK".equals(incident) &&
                "OK".equals(overlap);
        final var b2 = ("OK".equals(incident) ||
                "NON".equals(incident)) &&
                "OK".equals(overlap);
        final var b1 = "OK".equals(avail) && b2;
        return b || b1;
    }

    private String checkGeneralAvail(int teacherId, String day, String dayPart, int cohortId) {
        Cohort cohort = cohortRepository.getByCohortId(cohortId);
        String dayDayPart = day + "_" + dayPart;
        StaffAvailability staffAvailability = staffAvailrepo.getStaffAvailabilityByCohortAndId(cohort, teacherId);
        String status = switch (dayDayPart) {
            case "maandag_ochtend" -> staffAvailability.getMaandagOchtend();
            case "dinsdag_ochtend" -> staffAvailability.getDinsdagOchtend();
            case "woensdag_ochtend" -> staffAvailability.getWoensdagOchtend();
            case "donderdag_ochtend" -> staffAvailability.getDonderdagOchtend();
            case "vrijdag_ochtend" -> staffAvailability.getVrijdagOchtend();
            case "maandag_middag" -> staffAvailability.getMaandagMiddag();
            case "dinsdag_middag" -> staffAvailability.getDinsdagMiddag();
            case "woensdag_middag" -> staffAvailability.getWoensdagMiddag();
            case "donderdag_middag" -> staffAvailability.getDonderdagMiddag();
            case "vrijdag_middag" -> staffAvailability.getVrijdagMiddag();
            default -> DEFAULT;
        };
        return getResult(status);
    }

    private static String getResult(String status) {
        return switch (status) {
            case "JA" -> "OK";
            case "NEE" -> "NOK";
            default -> "NON";
        };
    }

    private String checkCohortOverlap(int cohortId, int teacherId, int weekNr, String dayPart, String weekDay) {
        User teacher = userRepo.findUserById(teacherId);
        CohortSchedule cohortSchedule = cohortScheduleRepo
                .findCohortScheduleByWeekNrAndDayPartAndDayAndUser(weekNr, dayPart, weekDay, teacher);
        final int scheduleId = (cohortId != cohortSchedule.getCohort().getCohortId()) ? 0 : cohortSchedule.getId();
        return scheduleId > 0 ? "NOK" : "OK";
    }

    private String checkSubjectPreference(int teacherId, int subjectId) {
        User user = userRepo.findUserById(teacherId);
        Subject subject = subjectRepository.findSubjectBySubjectId(subjectId);
        int preference  = subjectPreferenceRepository.findBySubjectAndUser(subject, user).getPreference();
        return switch (preference) {
            case 0 -> "NON";
            case 1 -> "NOK";
            case 2, 3 -> "OK";
            default -> DEFAULT;
        };
    }

    private String checkTeacherIncident(int teacherId, int weekNumber) {
        List<Exception> teacherExceptions = exceptionRepo.findAllByUserId(teacherId);
        if (teacherExceptions == null || teacherExceptions.isEmpty()) {
            return "NON";
        }
        String result = "NON";
        for (Exception exception : teacherExceptions) {
            LocalDate startdayDate = LocalDate.parse(exception.getStartDate());
            LocalDate enddayDate = LocalDate.parse(exception.getEndDate());
            WeekFields weekFields = WeekFields.of(Locale.getDefault());

            if (exception.getStartDate().equals(exception.getEndDate())) {
                result = getString(weekNumber, exception, startdayDate, enddayDate, weekFields);
            }
        }
        return result;
    }

    private static String getString(int weekNumber,
                                    Exception exception,
                                    LocalDate startDayDate,
                                    LocalDate enddayDate,
                                    WeekFields weekFields) {
        final var sameWeek = Stream.iterate(startDayDate, date -> date.isBefore(enddayDate), date -> date.plusDays(1))
                .mapToInt(date -> date.get(weekFields.weekOfWeekBasedYear()))
                .anyMatch(weekNr -> weekNr == weekNumber);
        if (sameWeek) {
            return "JA".equals(exception.getColorOption()) ? "OK" : "NOK";
        }
        return "NON";
    }

    private String checkTeacherHours(int teacherId, int subjectId, int cohortId) {
        TeacherHours teacherHours = teacherHoursRepository.findByUserId(teacherId);
        if (teacherHours == null) {
            return "NON";
        }
        if (!doesTeacherHaveExperienceWithSubject(teacherId, subjectId, cohortId)) {
            return teacherHours.getTeachingHoursLeft() < 6 ? "NOK" : "OK";
        }
        int yearsOfExperience = howManyYearsExperienceDoesTeacherHave(teacherId, subjectId, cohortId);
        final int realTeacherHours = switch (yearsOfExperience) {
            case 1 -> 8;
            case 2 -> 6;
            default -> 4;
        };
        return teacherHours.getTeachingHoursLeft() < realTeacherHours ? "NOK" : "OK";
    }

    private boolean doesTeacherHaveExperienceWithSubject(int teacherId, int subjectId, int cohortId) {
        List<CohortSchedule> cohortScheduleList = cohortScheduleRepo
                .getAllByUserIdAndSubject_SubjectIdAndCohort_CohortIdIsNot(teacherId, subjectId, cohortId);
        return !cohortScheduleList.isEmpty();
    }

    private int howManyYearsExperienceDoesTeacherHave(int teacherId, int subjectId, int cohortId) {
        List<CohortSchedule> cohortScheduleList = cohortScheduleRepo
                .getAllByUserIdAndSubject_SubjectIdAndCohort_CohortIdIsNot(teacherId, subjectId, cohortId);
        return switch (cohortScheduleList.size()) {
            case 1 -> 1;
            case 2 -> 2;
            default -> 3;
        };
    }

    @PostMapping(value = "/generateCohortSchedule/check")
    public String checkSchedule(HttpServletRequest request) {
        String buttonClicked = request.getParameter("button");
        if ("check".equals(buttonClicked)) {
            return "";
        }
        if ("save".equals(buttonClicked)) {
            int cohortId = Integer.parseInt(request.getParameter("cohortnr"));
            int subjectId = Integer.parseInt(request.getParameter("subjectnr"));
            int teacherId = Integer.parseInt(request.getParameter("teachernr"));
            String weekDay = request.getParameter("day");
            String dayPart = request.getParameter("daypart");
            LocalDate dayDate = LocalDate.parse(request.getParameter("dateDay"));
            WeekFields weekFields = WeekFields.of(Locale.getDefault());
            int weekNumber = dayDate.get(weekFields.weekOfWeekBasedYear());
            return totalCheck(cohortId, teacherId, dayPart, weekDay, subjectId, weekNumber);
        }
        return "";
    }

    @GetMapping(value = "/generateCohortSchedule")
    public String generateCohortSchedule() {
        return "generateCohortSchedule";
    }

    @PostMapping(value = "/subjectCohortCoupeling")
    public String subjectCohortCoupling() {
        return "OK";
    }
}
