package org.hva.controller;

import org.hva.model.UserSingleton;
import org.hva.model.entity.user.Role;
import org.hva.model.entity.user.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WelcomeScreenController {

    @RequestMapping("/welcomeScreen")
    public String welcome(Model model) {
        User userCurrent = UserSingleton.getInstance().getCurrentUser();
        String[] roles = userCurrent.getRolesOfUser().stream()
                .map(Role::getRoleName)
                .toArray(String[]::new);
        if (roles.length != 0) {
            model.addAttribute("roles", roles);
        }
        model.addAttribute("user", userCurrent);
        return "welcomeScreen";
    }

    @RequestMapping(value = "/helloAdmin")
    public String welcomeAdmin(Model model) {
        User currentUser = UserSingleton.getInstance().getCurrentUser();
        model.addAttribute("user", currentUser);
        return "helloAdmin";
    }

    @RequestMapping("/helloScheduler")
    public String welcomeScheduler(Model model) {
        User userCurrent = UserSingleton.getInstance().getCurrentUser();
        model.addAttribute("user", userCurrent);
        return "helloScheduler";
    }

    @RequestMapping("/helloTeacher")
    public String welcomeTeacher(Model model) {
        User userCurrent = UserSingleton.getInstance().getCurrentUser();
        model.addAttribute("user", userCurrent);
        return "helloTeacher";
    }

    @RequestMapping("/helloCoordinator")
    public String welcomeCoordinator(Model model) {
        User userCurrent = UserSingleton.getInstance().getCurrentUser();
        model.addAttribute("user", userCurrent);
        return "helloCoordinator";
    }
}
