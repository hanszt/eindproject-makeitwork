package org.hva.controller;

import org.hva.model.entity.Task;
import org.hva.repository.TaskRepository;
import org.springframework.core.task.TaskRejectedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/taskController")
public class TaskController {

    private final TaskRepository taskRepository;

    public TaskController(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @RequestMapping(value = "/showTasks")
    public String makeVacancyList(Model model) {
//        model.addAttribute("showTasks", taskRepository.getVacancies());
        return "showTasks";
    }

    @RequestMapping("/taskList")
    public String showTaskList(Model model){
        List<Task>tasks= taskRepository.findAll();
        model.addAttribute("tasks",tasks);
        return "task/task-list";
    }

    @RequestMapping("/newTask")
    public String addTask(Model model){
        Task task = new Task();
        model.addAttribute("task",task);
        return "task/task-form";
    }
    @PostMapping("/taskSave")
    public String addTask(@ModelAttribute ("task") Task task){
        taskRepository.save(task);
        return ("redirect:/taskList");
    }


    @GetMapping("/updateTask")
    public String updateSubject(@RequestParam("taskId") int taskId, Model model) {
        Task task = taskRepository.findTaskByTaskId(taskId)
                .orElseThrow(() -> new TaskRejectedException("No task found bu id: " + taskId));
        model.addAttribute("task", task);
        return "task/task-form";
    }

    @GetMapping("task/deleteTask")
    public String deleteSubject(@RequestParam("taskId") int taskId) {
        taskRepository.deleteById(taskId);
        return ("redirect:/taskList");
    }
}

