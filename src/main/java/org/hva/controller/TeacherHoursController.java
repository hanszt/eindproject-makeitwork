package org.hva.controller;

import org.hva.model.entity.Cohort;
import org.hva.model.entity.CohortSchedule;
import org.hva.model.entity.Subject;
import org.hva.model.entity.user.User;
import org.hva.model.UserSingleton;
import org.hva.repository.CohortRepository;
import org.hva.repository.CohortScheduleRepository;
import org.hva.repository.SubjectRepository;
import org.hva.repository.TeacherHoursRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class TeacherHoursController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TeacherHoursController.class);
    private final CohortScheduleRepository cohortScheduleRepository;
    private final CohortRepository cohortRepository;
    private final SubjectRepository subjectRepository;

    public TeacherHoursController(TeacherHoursRepository teacherHoursRepository,
                                  CohortScheduleRepository cohortScheduleRepository,
                                  CohortRepository cohortRepository,
                                  SubjectRepository subjectRepository) {
        this.cohortScheduleRepository = cohortScheduleRepository;
        this.cohortRepository = cohortRepository;
        this.subjectRepository = subjectRepository;
    }

    @GetMapping("teacherFTE")
    public String getTeacherFte(User user, Model model) {
        User currentUser = UserSingleton.getInstance().getCurrentUser();
        double userFTE = user.getFte();
        List<Cohort> cohorts = cohortRepository.findAllByCohortIdAfter(0);
        List<Subject> subjects = subjectRepository.findAll();
        model.addAttribute("voornaam", currentUser.getName());
        model.addAttribute("achternaam", currentUser.getLastName());
        model.addAttribute("email", currentUser.getEmail());
        model.addAttribute("fte", userFTE);
        model.addAttribute("cohorts", cohorts);
        model.addAttribute("vakkenSelectAjax", subjects);
        return "teacherFTE";
    }

    @RequestMapping(value = "teacherSchedule/{cohortId}", method = RequestMethod.GET)
    public @ResponseBody
    List<Integer> getTeacherSchedule(User user, Model model) {
        getTeacherFte(user, model);
        LOGGER.info("teacherschedule/{cohortid} is aangeroepen");
        List<Integer> distinctWeeknrsFromCohort = new ArrayList<>();
        LOGGER.info("Uit methode komt " + distinctWeeknrsFromCohort);
        return distinctWeeknrsFromCohort;
    }

    @RequestMapping(value = "teacherSchedule/week/{weeknr}/{cohortId}", method = RequestMethod.GET)
    public @ResponseBody
    List<CohortSchedule> getTeacherWeekSchedule(@PathVariable int weeknr, @PathVariable int cohortId, User user, Model model) {
        getTeacherFte(user, model);
        LOGGER.info("Uit mijn requestparam komt : " + weeknr + " en cohortId " + cohortId);
        List<CohortSchedule> cohortScheduleWeek = cohortScheduleRepository.getAllByCohort_CohortIdAndWeekNr(cohortId, weeknr);
        LOGGER.info("uit mijn cohortscheduleweek komt " + cohortScheduleWeek);
        return cohortScheduleWeek;
    }

    @RequestMapping(value = "/subject/{subjectId}")
    public @ResponseBody
    Subject getSubject(@PathVariable Integer subjectId) {
        LOGGER.info("MIJN ajax methode is aangeroepen");
        Subject subject = subjectRepository.findSubjectBySubjectId(subjectId);
        LOGGER.info("IN SUBJECT ZIT  " + subject);
        return subject;
    }
}
