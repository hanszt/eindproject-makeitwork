package org.hva.controller;

import org.hva.model.entity.Cohort;
import org.hva.model.entity.CohortSchedule;
import org.hva.repository.CohortRepository;
import org.hva.repository.CohortScheduleRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping(value = "/cohort")

public class CohortController {

    private static final String REDIRECT_COHORT_CREATE_COHORT = "redirect:/cohort/createCohort";
    private final CohortRepository cohortRepository;
    private final CohortScheduleRepository cohortScheduleRepo;

    public CohortController(CohortRepository cohortRepository, CohortScheduleRepository cohortScheduleRepo) {
        this.cohortRepository = cohortRepository;
        this.cohortScheduleRepo = cohortScheduleRepo;
    }

    @GetMapping("/createCohort")
    public String getAllCohorts(Model model) {
        List<Cohort> cohorts = cohortRepository.findAll();
        Collections.sort(cohorts);
        Cohort cohort = new Cohort();
        model.addAttribute("allCohorts", cohorts);
        model.addAttribute("cohort", cohort);
        model.addAttribute("cohortId", cohort.getCohortId());
        model.addAttribute("beginDate", cohort.getBeginDate());
        model.addAttribute("endDate", cohort.getEndDate());
        return "createCohort";
    }

    @PostMapping("/createCohort/new/")
    public String createCohort(@ModelAttribute("cohort") Cohort cohort) {
        boolean exists = cohortRepository.existsById(cohort.getCohortId());
        if (exists) {
            return "createCohortError";
        }
        cohort.setBeginDate(cohort.getBeginDate());
        cohort.setEndDate(cohort.getEndDate());
        cohortRepository.save(cohort);
        makeDefaultCohortSchedule(cohort.getBeginDate(), cohort.getEndDate(), cohort.getCohortId());
        return REDIRECT_COHORT_CREATE_COHORT;
    }

    private void makeDefaultCohortSchedule(LocalDate beginDate, LocalDate endDate, int cohortId) {
        for (LocalDate date = beginDate; date.isBefore(endDate); date = date.plusDays(1)) {
            Cohort cohort = cohortRepository.getByCohortId(cohortId);
            final var CsMorning = createSchedule(date, cohort, "ochtend");
            final var CsNoon = createSchedule(date, cohort, "middag");
            cohortScheduleRepo.save(CsMorning);
            cohortScheduleRepo.save(CsNoon);
        }
    }

    private static CohortSchedule createSchedule(LocalDate date, Cohort cohort, String dayPart) {
        return switch (date.getDayOfWeek()) {
            case MONDAY -> new CohortSchedule("maandag", dayPart, date, cohort);
            case TUESDAY -> new CohortSchedule("dinsdag", dayPart, date, cohort);
            case WEDNESDAY -> new CohortSchedule("woensdag", dayPart, date, cohort);
            case THURSDAY -> new CohortSchedule("donderdag", dayPart, date, cohort);
            case FRIDAY -> new CohortSchedule("vrijdag", dayPart, date, cohort);
            default -> throw new IllegalStateException("Unexpected value: " + date.getDayOfWeek());
        };
    }

    @PostMapping("/save")
    public String saveCohort(@ModelAttribute("cohort") Cohort cohort) {
        cohortRepository.save(cohort);
        return REDIRECT_COHORT_CREATE_COHORT;
    }

    @GetMapping("/deleteCohort")
    public String deleteSubject(@RequestParam("cohortId") int cohortId) {
        cohortRepository.deleteById(cohortId);
        return REDIRECT_COHORT_CREATE_COHORT;
    }

    @GetMapping("/updateCohort")
    public String updateCohort(@RequestParam("cohortId") int cohortId, Model model) {
        Cohort cohort = cohortRepository.getByCohortId(cohortId);
        model.addAttribute("cohort", cohort);
        return "cohortForm";
    }
}
