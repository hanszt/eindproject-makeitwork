package org.hva.controller;

import jakarta.servlet.http.HttpServletRequest;
import org.hva.model.UserSingleton;
import org.hva.model.entity.Task;
import org.hva.model.entity.TaskApplication;
import org.hva.repository.TaskApplicationRepository;
import org.hva.repository.TaskRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.LocalDate;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Ozgur
 */
@Controller
public class TaskApplicationController {

    private static final Pattern NR_REGEX = Pattern.compile("^\\d+$");
    private final TaskApplicationRepository taskApplicationRepo;
    private final TaskRepository taskRepository;

    public TaskApplicationController(TaskApplicationRepository taskApplicationRepo,
                                     TaskRepository taskRepository) {
        this.taskApplicationRepo = taskApplicationRepo;
        this.taskRepository = taskRepository;
    }

    @PostMapping(value = "/taskApplications/{taskId}/{availableHours}")
    public String insertTaskApplication(HttpServletRequest request, ModelMap model) {
        //testwaarde for userId is 1; totdat userid uit html gelezen kan worden
        LocalDate todaysDate = LocalDate.now();
        //get the data from httpservletRequest and put in variable
        String tempId = request.getParameter("taskId");
        String tempHours = request.getParameter("availableHours");
        if (NR_REGEX.matcher(tempHours).matches()) {
            //Convert variable to int
            int taskId = Integer.parseInt(tempId);
            int hours = Integer.parseInt(tempHours);
            //haal de userId op vd loggedin user uit de Singleton
            final int userId = UserSingleton.getInstance().getCurrentUser().getId();
            // insert the data into database
            taskRepository.findTaskByTaskId(taskId)
                    .ifPresent(task -> new TaskApplication(userId, todaysDate, hours, "Docent", task));
            // get latest data from db en send to showtasks.html
            List<Task> listWithAppliedForTasks = taskApplicationRepo.findAll().stream()
                    .map(TaskApplication::getTask)
                    .toList();
            List<Task> vacancyList = taskRepository.findAll();
            vacancyList.removeAll(listWithAppliedForTasks);
            model.addAttribute("showTasks", vacancyList);
            return "showTasks";
        } else {
            return "showTasksError";
        }
    }

    @PostMapping(value = "/taskApplications/{taskId}/{fullName}/{availHours}")
    public String updateTaskApplications(HttpServletRequest request) {
        //get the data from httpservletRequest and put in variable
        String tempId = request.getParameter("taskId");
        String tempHours = request.getParameter("availHours");
        String updateAction = request.getParameter("updateAppl");
        String deleteAction = request.getParameter("removeAppl");
        //Convert variable to int
        if (NR_REGEX.matcher(tempHours).matches()) {
            int taskId = Integer.parseInt(tempId);
            int hours = Integer.parseInt(tempHours);
            final int userId = UserSingleton.getInstance().getCurrentUser().getId();
            return "applicationBasket";
        }
        return "applicationBasketError";
    }


    @GetMapping(value = "/applicationBasket")
    public String fillApplicationBasket(Model model) {
        return "applicationBasket";
    }
}
