package org.hva.controller;

import org.hva.model.entity.ConfirmationToken;
import org.hva.model.entity.TeacherHours;
import org.hva.model.entity.user.Role;
import org.hva.model.entity.user.User;
import org.hva.repository.ConfirmationTokenRepository;
import org.hva.repository.RoleRepository;
import org.hva.repository.TeacherHoursRepository;
import org.hva.service.EmailSenderService;
import org.hva.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import jakarta.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/authController")
public class AuthenticationController {

    private static final int TOTAL_HOURS = 1650;
    private static final int EDUCATIONAL_PERCENTAGE = 80;
    private static final int DEFAULT_HOURS_USED = 0;
    private static final int DEFAULT_FTE = 1;
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationController.class);
    public static final String SUCCESS_MESSAGE = "successMessage";

    private final UserService userService;
    private final TeacherHoursRepository teacherHoursRepo;
    private final RoleRepository roleRepository;
    private final ConfirmationTokenRepository confirmationTokenRepository;
    private final EmailSenderService emailSenderService;
    private final BCryptPasswordEncoder encoder;

    public AuthenticationController(UserService userService,
                                    TeacherHoursRepository teacherHoursRepo,
                                    RoleRepository roleRepository,
                                    ConfirmationTokenRepository confirmationTokenRepository,
                                    EmailSenderService emailSenderService,
                                    BCryptPasswordEncoder encoder) {
        this.userService = userService;
        this.teacherHoursRepo = teacherHoursRepo;
        this.roleRepository = roleRepository;
        this.confirmationTokenRepository = confirmationTokenRepository;
        this.emailSenderService = emailSenderService;
        this.encoder = encoder;
    }

    @GetMapping("/login")
    public ModelAndView login() {
        LOGGER.info("Logging in...");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login"); // resources/template/login.html
        return modelAndView;
    }

    @PostMapping(value = "/register")
    public ModelAndView registerUser(@Valid User user, BindingResult bindingResult, ModelMap modelMap) {
        LOGGER.info("Registering user: {}", user);
        ModelAndView modelAndView = new ModelAndView();
        // Check for the validations
        if (bindingResult.hasErrors()) {
            modelAndView.addObject(SUCCESS_MESSAGE, "Graag de fout in het formulier verbeteren!");
            modelMap.addAttribute("bindingResult", bindingResult);
        } else if (userService.isUserAlreadyPresent(user)) {
            modelAndView.addObject(SUCCESS_MESSAGE, "Deze gebruiker bestaat al!");
        } else {
            userService.saveUser(user);
            int totalHours = DEFAULT_FTE * TOTAL_HOURS * EDUCATIONAL_PERCENTAGE / 100;
            TeacherHours newTeacherHours = new TeacherHours(user.getId(), totalHours, DEFAULT_HOURS_USED, totalHours);
            teacherHoursRepo.save(newTeacherHours);
            modelAndView.addObject(SUCCESS_MESSAGE, "Gebruiker succesvol toegevoegd!");
        }
        List<Role> roleList = roleRepository.findAll();
        modelAndView.addObject("roles", roleList);
        modelAndView.addObject("user", new User());
        modelAndView.setViewName("user/newUser");
        return modelAndView;
    }

    @GetMapping( "/home")
    public ModelAndView home() {
        LOGGER.info("Welcome to the home page");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("helloTeacher");
        return modelAndView;
    }

    /**
     * Display the forgot password page and form
     */
    @GetMapping( "/forgot-password")
    public ModelAndView displayResetPassword(ModelAndView modelAndView, User user) {
        modelAndView.addObject("user", user);
        modelAndView.setViewName("forgotPassword");
        return modelAndView;
    }

    /**
     * Receive email of the user, create token and send it via email to the user
     */
    @PostMapping( "/forgot-password")
    public ModelAndView forgotUserPassword(ModelAndView modelAndView, User user) {
        final var existingUser = userService.findByEmail(user.getEmail()).orElse(null);
        if (existingUser != null) {
            // create token
            ConfirmationToken confirmationToken = new ConfirmationToken(existingUser);
            // save it
            confirmationTokenRepository.save(confirmationToken);
            // create the email
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(existingUser.getEmail());
            mailMessage.setSubject("Complete Password Reset!");
            mailMessage.setFrom("nopressure532@gmail.com");
            mailMessage.setText("To complete the password reset process, please click here: "
                    + "http://localhost:8081/confirm-reset?token=" + confirmationToken.getConfirmationToken());
            emailSenderService.sendEmail(mailMessage);
            modelAndView.addObject("message", "Het verzoek om het wachtwoord te resetten is ontvangen.\n" +
                    "Check je inbox en klik op de link om je wachtwoord te resetten.");
            modelAndView.setViewName("successForgotPassword");
        } else {
            modelAndView.addObject("message", "Dit emailadres bestaat niet!");
            modelAndView.setViewName("error1");
        }
        return modelAndView;
    }


    @RequestMapping(value = "/confirm-reset", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView validateResetToken(ModelAndView modelAndView, @RequestParam("token") String confirmationToken) {
        ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);
        if (token != null) {
            User user = userService.findByEmail(token.getUser().getEmail()).orElseThrow();
            user.setEnabled(true);
            userService.saveUser(user);
            modelAndView.addObject("user", user);
            modelAndView.addObject("email", user.getEmail());
            modelAndView.setViewName("resetPassword");
        } else {
            modelAndView.addObject("message", "De link is kapot of ongeldig!");
            modelAndView.setViewName("error1");
        }
        return modelAndView;
    }

    /**
     * Receive the token from the link sent via email and display form to reset password
     */
    @PostMapping( "/reset-password")
    public ModelAndView resetUserPassword(ModelAndView modelAndView, User user) {
        if (user.getEmail() != null) {
            // use email to find user
            User tokenUser = userService.findByEmail(user.getEmail()).orElseThrow();
            tokenUser.setEnabled(true);
            tokenUser.setPassword(encoder.encode(user.getPassword()));
            userService.updateUser(tokenUser);
            modelAndView.addObject("message", "Wachtwoord succesvol gereset.\n" +
                    "Je kunt nu met je nieuwe wachtwoord inloggen.");
            modelAndView.setViewName("successResetPassword");
        } else {
            modelAndView.addObject("message", "De link is kapot of ongeldig!");
            modelAndView.setViewName("error1");
        }
        return modelAndView;
    }
}
