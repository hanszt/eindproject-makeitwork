package org.hva.controller;

import org.hva.model.UserSingleton;
import org.hva.model.entity.Cohort;
import org.hva.model.entity.StaffAvailability;
import org.hva.repository.CohortRepository;
import org.hva.repository.StaffAvailabilityRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/schedule")

public class StaffAvailabilityController {

    private final StaffAvailabilityRepository staffAvailabilityService;
    private final CohortRepository cohortRepository;

    public StaffAvailabilityController(StaffAvailabilityRepository staffAvailabilityRepository,
                                       CohortRepository cohortRepository) {
        this.staffAvailabilityService = staffAvailabilityRepository;
        this.cohortRepository = cohortRepository;
    }

    @RequestMapping("/list/{cohort}")
    public String listStaffAvailibilityByCohort(Model model, @PathVariable int cohort) {
        int userId = UserSingleton.getInstance().getCurrentUser().getId();
        List<StaffAvailability> schedule = staffAvailabilityService.findByUser_IdAndCohort_CohortId(userId, cohort);
        model.addAttribute("schedule", schedule);
        return ("schedule/list-schedule");
    }

    @GetMapping("/addSchedule")
    public String addSchedule(Model model) {
        StaffAvailability staffAvailability = new StaffAvailability();
        List<Cohort> cohortList = cohortRepository.findAll();
        List<StaffAvailability> staffAvailabilityList = staffAvailabilityService
                .findAllById(UserSingleton.getInstance().getCurrentUser().getId());

        List<Cohort> cohortsHavingSchedule = staffAvailabilityList.stream()
                .map(StaffAvailability::getCohort)
                .toList();

        cohortList.removeAll(cohortsHavingSchedule);

        List<Integer> cohortIdList = cohortList.stream()
                .map(Cohort::getCohortId)
                .collect(Collectors.toList());

        model.addAttribute("schedules", staffAvailability);
        model.addAttribute("cohorts", cohortIdList);
        return "schedule/schedule-form";
    }

    @PostMapping("/save")
    public String saveSchedule(@ModelAttribute("schedule") StaffAvailability sa) {
        staffAvailabilityService.save(sa);
        return ("redirect:/schedule/listCohort");
    }

    @GetMapping("/updateSchedule")
    public String updateSchedule(@RequestParam("scheduleId") int theId, Model model) {
        StaffAvailability sa = staffAvailabilityService.findById(theId);
        List<Cohort> cohorts = cohortRepository.findAll();
        model.addAttribute("schedule", sa);
        model.addAttribute("cohorts", cohorts);
        return "schedule/schedule-updateform";
    }

    @RequestMapping("/listCohort")
    public String listCohort(Model model) {
        List<Cohort> cohorts = cohortRepository.findAll();
        model.addAttribute("cohorts", cohorts);
        return ("schedule/schedule-cohortlist");
    }
}
