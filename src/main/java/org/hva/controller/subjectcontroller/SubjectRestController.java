package org.hva.controller.subjectcontroller;

import org.hva.model.entity.CohortSchedule;
import org.hva.model.entity.Subject;
import org.hva.repository.CohortScheduleRepository;
import org.hva.repository.SubjectRepository;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Ozgur
 */
@RestController
@RequestMapping("/vak")
public class SubjectRestController {

    private final SubjectRepository subjectRepository;
    private final CohortScheduleRepository cohortRepository;

    public SubjectRestController(SubjectRepository subjectRepository, CohortScheduleRepository cohortRepository) {
        this.subjectRepository = subjectRepository;
        this.cohortRepository = cohortRepository;
    }

    @RequestMapping("/lijst")
    public List<Subject> findAll() {
        return subjectRepository.findAll();
    }

    @PostMapping("/addSubject")
    public Subject addSubject(@RequestBody Subject subject) {
        subjectRepository.save(subject);
        return subject;
    }

    @DeleteMapping("/deleteSubject/{id}")
    public void deleteSubject(@PathVariable int id) {
        subjectRepository.deleteById(id);
    }

    @RequestMapping("/subjectCohortKopelen/{cohortId}")
    public List<CohortSchedule> findAllByCohortId(@PathVariable int cohortId) {
        List<CohortSchedule> cohortSchedules;
        cohortSchedules = cohortRepository.getAllByCohort_CohortId(cohortId);
        return cohortSchedules;
    }
}
