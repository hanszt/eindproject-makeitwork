package org.hva.controller.subjectcontroller;

import org.hva.model.entity.Cohort;
import org.hva.model.entity.CohortSchedule;
import org.hva.model.entity.Subject;
import org.hva.repository.CohortRepository;
import org.hva.repository.CohortScheduleRepository;
import org.hva.repository.SubjectRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static java.util.Comparator.comparing;

/**
 * @author Ozgur
 */
@Controller

public class SubjectWebController {
    private final SubjectRepository subjectRepository;
    private final CohortRepository cohortRepository;
    private final CohortScheduleRepository cohortScheduleRepository;
    private List<Subject> subjectList;

    public SubjectWebController(SubjectRepository subjectRepository, CohortRepository cohortRepository, CohortScheduleRepository cohortScheduleRepository) {
        this.subjectRepository = subjectRepository;
        this.cohortRepository = cohortRepository;
        this.cohortScheduleRepository = cohortScheduleRepository;
    }

    @GetMapping("/")
    public String subjectHomePage() {
        return "subject/subject-home";
    }

    @RequestMapping("/subject/list")
    public String listSubject(Model model) {
        subjectList = subjectRepository.findAll();
        subjectList.sort(comparing(Subject::getSubjectName, String::compareToIgnoreCase));
        List<Cohort> cohortList = cohortRepository.findAll();
        model.addAttribute("subjects", subjectList);
        model.addAttribute("cohorts", cohortList);
        return "subject/list-subject";
    }

    @GetMapping("/subject/addSubject")
    public String addSubject(Model model) {
        Subject subject = new Subject();
        model.addAttribute("subject", subject);
        return "subject/subject-form";
    }

    @PostMapping("/subject/save")
    public String saveSubject(@ModelAttribute("subject") Subject subject) {
        subjectRepository.save(subject);
        return ("redirect:/subject/list");
    }

    @GetMapping("/subject/updateSubject")
    public String updateSubject(@RequestParam("subjectId") int subjectId, Model model) {
        Subject subject = subjectRepository.findSubjectBySubjectId(subjectId);
        model.addAttribute("subject", subject);
        return "subject/subject-form";
    }

    @GetMapping("subject/deleteSubject")
    public String deleteSubject(@RequestParam("subjectId") int subjectId) {
//        cohortScheduleRepository.nullSubjectId(subjectId);
        subjectRepository.deleteById(subjectId);
        return ("redirect:/subject/list");
    }

    @GetMapping("/subject/subjectCohortKopelen/{cohortId}")
    public String getAllCohortSchedule(Model model, @PathVariable Integer cohortId) {
        subjectList = subjectRepository.findAll();
        List<CohortSchedule> cohortSchedules = cohortScheduleRepository.getAllByCohort_CohortId(cohortId);
        model.addAttribute("cohortsSchedules", cohortSchedules);
        model.addAttribute("subjects", subjectList);
        return ("list-cohortSchedule");
    }
}
